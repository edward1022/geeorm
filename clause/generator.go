package clause

import (
	"fmt"
	"strings"
)

type generator func(values ...interface{}) (string, []interface{})

var generators map[Type]generator

func init() {
	generators = make(map[Type]generator)
	generators[INSERT] = _insert
	generators[VALUES] = _values
	generators[SELECT] = _select
	generators[WHERE] = _where
	generators[LIMIT] = _limit
	generators[ORDERBY] = _orderby
	generators[UPDATE] = _update
	generators[DELETE] = _delete
	generators[COUNT] = _count
}

func _update(values ...interface{}) (string, []any) {
	// update $1 set name = ?, bbb
	tableName := values[0]
	// map[string]interface{}
	m := values[1].(map[string]interface{})
	var keys []string
	var vars []interface{}
	for k, v := range m {
		keys = append(keys, k+" = ?")
		vars = append(vars, v)
	}
	return fmt.Sprintf("update %s set %s", tableName, strings.Join(keys, ", ")), vars
}

func _delete(values ...interface{}) (string, []any) {
	//delete from $1
	return fmt.Sprintf("delete from %s", values[0]), []any{}
}

func _count(values ...interface{}) (string, []any) {
	// select count(*) from $1
	return _select(values[0], []string{"count(*)"})
}

func _insert(values ...interface{}) (string, []any) {
	// insert into $tableName ($fields)
	tableName := values[0]
	fields := strings.Join(values[1].([]string), ",")
	return fmt.Sprintf("INSERT INTO %s (%v)", tableName, fields), []any{}
}

func genBindVars(num int) string {
	var vars []string
	for i := 0; i < num; i++ {
		vars = append(vars, "?")
	}
	return strings.Join(vars, ", ")
}

func _values(values ...interface{}) (string, []any) {
	// values ($v1), $(v2)
	var bindStr string
	var sql strings.Builder
	var vars []any
	sql.WriteString("VALUES ")
	for i, value := range values {
		v := value.([]any)
		if bindStr == "" {
			bindStr = genBindVars(len(v))
		}
		sql.WriteString(fmt.Sprintf("(%v)", bindStr))
		if i+1 != len(values) {
			sql.WriteString(", ")
		}
		vars = append(vars, v...)
	}
	return sql.String(), vars
}

// select * from vc_user where '`name` = ?', tom
func _select(values ...interface{}) (string, []any) {
	//select $fields from $tableName
	tableName := values[0]
	fields := strings.Join(values[1].([]string), ",")
	return fmt.Sprintf("SELECT %v FROM %s", fields, tableName), []any{}
}

func _limit(values ...interface{}) (string, []any) {
	// limit $num
	return "LIMIT ? ", values
}

func _where(values ...interface{}) (string, []any) {
	// where $desc
	desc, vars := values[0], values[1:]
	return fmt.Sprintf("WHERE %s", desc), vars
}

func _orderby(values ...interface{}) (string, []any) {
	return fmt.Sprintf("ORDER BY %s", values[0]), []interface{}{}
}
