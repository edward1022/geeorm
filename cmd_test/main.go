package cmd_test

import (
	"fmt"
	"geeorm/geeorm"

	_ "github.com/mattn/go-sqlite3"
)

func main1() {
	engine, _ := geeorm.NewEngine("sqlite3", "gee.db")
	defer engine.Close()
	s := engine.NewSession()
	_, _ = s.Raw("DROP TABLE IF EXISTS User;").Exec()
	_, _ = s.Raw("CREATE TABLE User(Name text);").Exec()
	_, _ = s.Raw("CREATE TABLE User(Name text);").Exec()
	result, _ := s.Raw("INSERT into User(`name`) values(?),(?)", "tom", "sam").Exec()
	count, _ := result.RowsAffected()
	fmt.Printf("exec success, %d affect \n", count)
}
