package cmd_test

import (
	"geeorm/geeorm"
	"testing"
)

type User struct {
	Name string `geeorm:"PRIMARY KEY"`
	Age  int
}

func TestSession_CreateTable(t *testing.T) {
	engine, _ := geeorm.NewEngine("sqlite3", "gee.db")
	defer engine.Close()
	s := engine.NewSession().Model(&User{})
	_ = s.DropTable()
	_ = s.CreateTable()
	if !s.HasTable() {
		t.Fatal("failed to create table User")
	}

}

func Test_Insert(t *testing.T) {
	engine, _ := geeorm.NewEngine("sqlite3", "gee.db")
	s := engine.NewSession()
	u1 := &User{Name: "Tom", Age: 18}
	u2 := &User{Name: "Sam", Age: 25}
	s.Insert(u1, u2)

}
