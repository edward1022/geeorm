package main

import (
	"fmt"
	"geeorm/geeorm"

	_ "github.com/mattn/go-sqlite3"
)

type User struct {
	Name string
	Age  int
}

func main() {
	engine, _ := geeorm.NewEngine("sqlite3", "gee.db")
	defer engine.Close()
	s := engine.NewSession()
	_, _ = s.Raw("DROP TABLE IF EXISTS User;").Exec()
	_, _ = s.Raw("CREATE TABLE User(Name text,Age int(11));").Exec()
	_, _ = s.Raw("CREATE TABLE User(Name text,Age int(11));").Exec()
	result, _ := s.Raw("INSERT into User(`name`,`Age`) values(?,?),(?,?)", "tom", 11, "sam", 12).Exec()
	count, _ := result.RowsAffected()
	fmt.Printf("exec success, %d affect \n", count)

	engine2, _ := geeorm.NewEngine("sqlite3", "gee.db")
	defer engine2.Close()
	s2 := engine2.NewSession()
	u1 := &User{Name: "Tom", Age: 18}
	u2 := &User{Name: "Sam", Age: 25}
	s2.Insert(u1, u2)

	s3 := engine2.NewSession()
	//find
	var users []User
	s3.Find(&users)
	for _, v := range users {
		fmt.Printf("%+v\n", v)
	}

	s4 := engine2.NewSession()
	u := &User{}
	err := s4.First(u)
	fmt.Sprintf("%+v", err)
	fmt.Sprintf("%+v\n", u)
	fmt.Printf("len", len(users))

	s5 := engine2.NewSession()
	count2, _ := s5.Model(u).Where("Name = ?", "Tom").Count()
	fmt.Printf("%+d", count2)
}
