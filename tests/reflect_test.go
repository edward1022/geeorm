package tests

import (
	"fmt"
	"reflect"
	"testing"
)

type myInt int64

func reflectType(t *testing.T, x any) {
	typ := reflect.TypeOf(x)
	t.Logf("name:%v kind:%v", typ.Name(), typ.Kind())
}

func reflectValue(t *testing.T, x any) {
	v := reflect.ValueOf(x)
	k := v.Kind()
	switch k {
	case reflect.Int64:
		// v.Int()从反射中获取整型的原始值，然后通过int64()强制类型转换
		t.Logf("type is int64, value is %d\n", int64(v.Int()))
	case reflect.Float32:
		// v.Float()从反射中获取浮点型的原始值，然后通过float32()强制类型转换
		t.Logf("type is float32, value is %f\n", float32(v.Float()))
	case reflect.Float64:
		// v.Float()从反射中获取浮点型的原始值，然后通过float64()强制类型转换
		t.Logf("type is float64, value is %f\n", float64(v.Float()))

	}
}

func TestReflect(t *testing.T) {
	// var a float32 = 3.14
	// v1 := reflect.TypeOf(a)
	// t.Logf("type:%v\n", v1) // type:float32

	// var b int64 = 100
	// v2 := reflect.TypeOf(b)
	// t.Logf("type:%v\n", v2) // type:int64

	var a *float32     // 指针
	var b myInt        // 自定义类型
	var c rune         // 类型别名
	var c1 byte        // 类型别名
	var f string       // 字符串
	var g int          // 整型
	var h bool         // 布尔型
	reflectType(t, a)  // name: kind:ptr
	reflectType(t, b)  // name:myInt kind:int64
	reflectType(t, c)  // name:int32 kind:int32
	reflectType(t, c1) // name:uint8 kind:uint8
	reflectType(t, f)  // name:string kind:string
	reflectType(t, g)  // name:int kind:int
	reflectType(t, h)  // name:bool kind:bool

	type person struct {
		name string
		age  int
	}
	type book struct{ title string }
	var d = person{
		name: "沙河小王子",
		age:  18,
	}
	var e = book{title: "《跟小王子学Go语言》"}
	reflectType(t, d) // type:person kind:struct
	reflectType(t, e) // type:book kind:struct

	type tt interface {
	}
	var k tt
	ttt := reflect.TypeOf(k)
	t.Log(ttt)

	var aa float32 = 3.14
	var bb int64 = 100
	v := reflect.ValueOf(aa)
	kk := v.Kind()
	fmt.Println("55555", v, kk)

	reflectValue(t, aa) // type is float32, value is 3.140000
	reflectValue(t, bb) // type is int64, value is 100
}
